package utilities;


import com.tngtech.java.junit.dataprovider.DataProvider;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

public class readJSON {

    @DataProvider()
    public String[] test() throws IOException, ParseException {
        JSONParser jsonParser = new JSONParser();
        FileReader fileReader = new FileReader(".resources\\drivers\\testData\\SignUp.json");
        Object obj = jsonParser.parse(fileReader);

        JSONObject userloginsJSONobj = (JSONObject) obj;
        JSONArray userLoginArray = (JSONArray) userloginsJSONobj.get("userlogins");

        String arr[] = new String[userLoginArray.size()];

        for (int i = 0; i < userLoginArray.size(); i++) {

            JSONObject users = (JSONObject) userLoginArray.get(i);
            String user = (String) users.get("firstname");
            String pwd = (String) users.get("password");

            arr[i] = user + "," + pwd;
        }
        return arr;
    }

}
