package Test_FrontendPage;

import Frontend_Page.HomePage;
import org.json.simple.parser.ParseException;
import org.junit.Test;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.*;

import java.io.IOException;

public class Test_HomePage extends BaseClass {

    @Test
    public void test() throws InterruptedException, IOException, ParseException {

        HomePage homePage = new PageFactory().initElements(driver, HomePage.class);
        Thread.sleep(5000);
        WorkWithElements.clickElement(homePage.getdropdownAccount());
        WorkWithElements.clickElement(homePage.getSignUp());
        WorkWithElements.complete(homePage.getFirstName(),TextResources.firstname);
    }
}
